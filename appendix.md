This open letter has been written in response to the original letter against Richard Stallman in https://rms-open-letter.github.io/

A pull request https://github.com/rms-open-letter/rms-open-letter.github.io/pull/485/files was sent asking for our reply to be included. It was immediately rejected.

----

Esta carta abierta ha sido escrita en respuesta a la carta original en contra de Richard Stallman en https://rms-open-letter.github.io/

Fue enviado un pull request https://github.com/rms-open-letter/rms-open-letter.github.io/pull/485/files solicitando que nuestra réplica fuera incluída. Fue rechazado inmediatamente.

----

There's another open letter in https://github.com/rms-support-letter/rms-support-letter.github.io .  Support signatures are received via merge requests.

----
Two interesting links posted on  FSForce mailing list:
----

Hello

Two interesting links, via Alessandro Vesely on Devuan's list <dng@lists.dyne.org>:

An orthodox analysis entitled Justice for Dr. Richard Matthew Stallman, which recaps the whole story.
https://jorgemorais.gitlab.io/justice-for-rms/

A post, written by Hannah Wolfman-Jones, with a response from civil-rights expert Nadine Strossen, former president of the ACLU.
https://www.wetheweb.org/post/cancel-we-the-web

Footnote: Devuan is the non-systemd fork of Debian. It seems that in Devuan there is general agreement on supporting the letter in favor of Richard (thread "FSF and human rights" at https://lists.dyne.org/lurker/list/dng.en.html ). On the other hand, in Debian it is proposed to vote in less than a week to sign the letter against Richard although there are proposals against it (see https://lists.debian.org/debian-vote/2021/03/threads.html#00083 below). In https://rms-support-letter.github.io/index.html I see several Debian developers, while https://rms-open-letter.github.io/ it can be seen that several of the people who have or have had important positions in Debian also have positions in OSI (whose raison d'être is expressly to compete against the community in favor of "free software" including the FSFoundation seeking to impose "open source" over "free software", therefore very notorious the double affiliation Debian-OSI considering that point 1 of the Debian Constitution states "The Debian Project is an association of individuals who have made common cause to create a free operating system.") and in the two largest anti-Richard organizations: GNOME and SFConservancy.

----

Hola

Dos enlaces interesantes, vía Alessandro Vesely en la lista de Devuan <dng@lists.dyne.org>:

Un análisis ortodoxo titulado Justicia para el Dr. Richard Matthew Stallman, que resume toda la historia.
https://jorgemorais.gitlab.io/justice-for-rms/

Un post, escrito por Hannah Wolfman-Jones, con una respuesta de la experta en derechos civiles Nadine Strossen, ex presidenta de la ACLU.
https://www.wetheweb.org/post/cancel-we-the-web

Nota al pie: Devuan es el fork sin systemd de Debian. Parece que en Devuan hay acuerdo general sobre apoyar la carta a favor de Richard (hilo "FSF and human rights" en https://lists.dyne.org/lurker/list/dng.en.html ). Por su parte, en Debian está propuesta votar en menos de una semana la firma de la carta contra Richard aunque hay propuestas en contra (ver https://lists.debian.org/debian-vote/2021/03/threads.html#00083 hacia abajo). En https://rms-support-letter.github.io/index.html veo varios desarrolladores de Debian, mientras que https://rms-open-letter.github.io/ puede comprobarse que varios de las personas que tienen o han tenido puestos importantes en Debian también tienen cargos en la OSI (cuya razón de ser es expresamente competir contra la comunidad a favor del "software libre" incluida la FSFoundation buscando imponer "open source" sobre "software libre", por tanto muy notoria la doble afiliación Debian-OSI considerando que el punto 1 de la Constitución de Debian establece "El Proyecto Debian es una asociación de individuos que han hecho causa común para crear un sistema operativo libre.") y en las dos mayores organizaciones anti-Richard: GNOME and SFConservancy.

----
Fight against idiocy; support RMS https://linuxreviews.org/Fight_against_idiocy;_support_RMS
----


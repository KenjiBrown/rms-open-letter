*2021-03-23*

Richard Stallman is being unfairly lynched and defamed by a group of people in an open letter to the FSF. They are asking for him to be expelled from an organization he created, he commited his live to, and he deserves more than any one of us to be head of. 

We, the undersigned, belive in freedom as a human right. Freedom of speach is what Richard Stallman is being attacked for. In order to realize the promise of everything freedom makes possible, we must defend free speech and we must defend our community members against these kind of attacks to free speech based on false acusations and regrettable hate campaigns. Such hate campaigns as we know only lead to division and self destruction.

We acknowledge that some of Richard Stallman's opinions are controversial. Some of us might have different opinions, but we are mature enough to differentiate an opinion from bad behaviour. Commenting about a controversial topic is not a crime and should never be a crime. Commenting about a controversial topic should have never been a reason for Richard Stallman (or any one else) to lose his job, his reputation, his position and access to the projects he has been devoted to most of his life.

We accept and recognize Richard as a human being with strengths and weaknesses as any one else. We are thankful for all the work he has done in the past years in favor of software freedom. We acknowledge the need for more people like Richard in the head of key organizations like the Free Software Foundation and the GNU project.

There has been enough damage against Richard Stallman, the Free Software Foundation and the Free software movement as a whole. We have had enough. We have had enough *ad-hóminem* attacks targeted against Free Software representatives. We have had enough divisive attacks aiming to leave our Free Software movement weaker than ever before.

**We are calling for the endorsement of Richard M. Stallman as a Board member of the Free Software Foundation.** FSF members should recognize Richard for his commitment to software freedom and for his great job all these years. They should embrace freedom as a whole, despite of malicious dafamation attacks comming from people with personal interests against the person and the movement. It is time for RMS to recover his lost position and never step back again. Free Software movement needs more people with such a commitment for Free Software's ideals. 

**We are also calling for Richard M. Stallman to be restored to his original position as FSF's president and keep his current position in the GNU Project.** 

We urge those in a position to do so to keep supporting Richard Stallman and his return to the Free Software Foundation. Keep contributing to projects related to Free Software and RMS. Keep attending to Free Software events, and events that welcome RMS and other Free Software representatives. We ask for contributors to Free Software projects to take a stand against bigotry and hate within their projects. While doing these things, tell these communities the value of Freedom. 

We acknowledge all of Richard Stallman's contributions to software freedom. Some of us have our own stories about RMS and our interactions with him. We have welcomed him in our homes and our universities. We have learned to know him as the human he is, with his strengths and weaknesses, things that are not captured in email threads or on video. We hope you will read what has been shared and consider the harm some malicious people are doing to our community.

----

Richard Stallman está siendo linchado y difamado por un grupo de personas en una carta abierta a la FSF. Están pidiendo que sea expulsado de una organización que él creó, que dedicó su vida a ella y que merece más que cualquiera de nosotros estar a la cabeza.

Nosotros, los aquí firmantes, creemos en la libertad como un derecho humano. Libertad de expresión es por lo que Richard Stallman ha estado siendo atacado. Para poder alcanzar la promesa de todo lo que la libertad hace posible, debemos defender la libertad de expresión y debemos defender a los miembros de nuestra comunidad contra este tipo de ataques contra la libre expresión basados en falsas acusaciones y lamentables campañas de odio. Estas campañas de odio, como sabemos, solo nos llevan a la división y a la autodestrucción.

Reconocemos que algunas de las opiniones de Richard Stallman son controvertidas. Algunos de nosotros podríamos tener distintas opiniones, pero ya estamos lo suficientemente maduros para poder distinguir entre una opinión y un mal comportamiento. Opinar sobre temas polémicos no es un crimen y nunca debería serlo. Haber opinado sobre un tema polémico nunca debió haber sido razón para que Richard Stallman (o cualquier otro) perdiera su empleo, su reputación, su posición y acceso a proyectos a los que ha dedicado parte de su vida.

Aceptamos y reconocemos a Richard Stallman como un ser humano con virtudes y defectos como cualquier otro. Estamos agradecidos por todo el trabajo que ha hecho en estos años en favor de la libertad del software. Reconocemos la necesidad de que más personas como Richard estén a la cabeza de organizaciones clave como al Free Software Foundation y el proyecto GNU.

Ya ha habido suficiente daño en contra de Richard Stallman, la free Software Foundation y el movimiento de Software Libre como un todo. Ya tuvimos suficiente. Ya hemos tenido suficientes ataques *ad-hóminem* en contra de representantes del Software Libre. Ya hemos tenido suficientes ataques divisivos que solo buscan dejar a nuestro movimiento del Software Libre más débil que nunca antes.

**Estamos haciendo un llamado a respaldar a Richard M. Stallman como miembro de la Junta de la Free Software foundation.** Los miembros de la FSF deberían reconocer a Richard por su compromiso hacia la libertad del software y por su gran trabajo todos estos años. Deberían abrazar la libertad como un todo, a pesar de ataques malintencionados provenientes de personas con intereses personales en contra de la persona y del movimiento. Es hora de que RMS recupere su posición perdida y nunca vuelva a retroceder. El movimiento del Software Libre necesita más personas con ese compromiso hacia los ideales del Software Libre.

**Estamos haciendo un llamado para que Richard M. Stallman sea devuelto a su posición original como presidente de la FSF y mantenga su posición actual en el proyecto GNU.**

Instamos a quienes estén en posición de hacerlo, a que sigan apoyando a Richard Stallman y su regreso a la Free Software Foundation. Que sigan contribuyendo en proyectos relacionados con el Software Libre y RMS. Que sigan asistiendo a eventos de Software Libre, a eventos que reciban a RMS y otros representantes del Software Libre. Pedimos a quienes contribuyen en proyectos de Software Libre que tomen una posición en contra de la intolerancia y el odio hacia dentro de sus proyectos.  Y mientras lo hagan, que hablen a estas comunidades sobre el valor de la libertad.

Reconocemos todas las contribuciones de Richard Stallman a la libertad del software. Algunos de nosotros tenemos nuestras propias historias sobre Richard y cómo hemos convivido con él. lo hemos recibido en nuestras casas y en nuestras universidades. hemos aprendido a conocerlo como el ser humano que es, con sus virtudes y sus defectos, cosas que no pueden ser capturadas en listas de correo o en video. Esperamos que lean cuidadosamente lo que ha sido compartido y que consideren el daño que gente malintencionada está haciendo a nuestra comunidad.

----

To sign, please email <rms-support@softwarelibre.mx> or [submit a merge request](https://gitlab.com/KenjiBrown/rms-open-letter/-/merge_requests/new).

Institutional affiliation is provided for identification purposes only and does not constitute institutional endorsement.

----
Signed in support for Richard M Stallman,
----

- Adrian Zaugg (Sat, 27 Mar 2021 10:59:01 +0100)
- Alexis Puente Montiel, libre software activist (organizator of the first FLISOL in Europe and Spanish coordinator of FLISOL) (Thu, 25 Mar 2021 20:51:00 +0000)
- Cori Santander (Free Software Activist) (Wed, 24 Mar 2021 12:56:56 -0400)
- Donato Molino (RMS Supporter) (Thu, 25 Mar 2021 17:39:25 +0100)
- Eduardo Valdivia Lugo <evaldivia@riseup.net> (53d762a37317e753f0e5e7141c82904587fddf64)
- Emilio Lo Prete - Personal Website http://elp.sdf.org Reddit https://www.reddit.com/r/desilusionenlacolmena (Tue, 30 Mar 2021 16:05:33 -0300)
- Emmanuel Florac I support RMS (I'm FSF member since 2002) (Thu, 25 Mar 2021 20:27:26 +0100)
- figosdev <faiflab@tutanota.com> (54cf084ba17817ed68e536f0e22ea520d89a8802)
- Fred E <mett@pmars.jp> (Sun, 28 Mar 2021 14:02:24 +0900)
- Javier Obregón - Posadas Misiones Argentina (Wed, 24 Mar 2021 17:05:03 +0000)
- Guillermo Molleda Jimena (Wed, 24 Mar 2021 11:59:24 +0100)
- Iván Ruvalcaba (Libre software enthusiast) (Thu, 25 Mar 2021 13:02:12 -0600)
- Juan Carlos Gentile <jucar@hipatia.net> (Fri, 26 Mar 2021 04:07:20 +0100)
- Leah Rowe of Libreboot. See https://libreboot.org/ (Wed, 24 Mar 2021 21:39:13 +0000)
- Marcel Ventosa (Wed, 24 Mar 2021 05:48:58 -0600)
- Matías Bobadilla Píriz Soy un miembro asociado de la FSF. (Thu, 25 Mar 2021 10:25:33 +0000)
- Matías Croce (Wed, 24 Mar 2021 14:38:00 -0300) (0173e7c5b877e8c36054d8fca18a17d0e2f1188a)
- Miquel Coma Rivas (Thu, 25 Mar 2021 10:33:14 +0100)
- Noir Blanichi (Thu, 25 Mar 2021 13:33:05 +1300)
- Oscar Ibarra H. (Wed, 24 Mar 2021 15:34:48 +0000)
- PicaHack http://picahack.org (Tue, 30 Mar 2021 21:19:47 +0200)
- Reynaldo Cordero Corro. Soy un miembro asociado de la FSF. (Sat, 27 Mar 2021 13:44:09 +0000)
- Ricardo Morte Ferrer, Abogado, miembro de la FSF (Fri, 26 Mar 2021 15:51:35 +0100)
- Sandino Araico Sánchez <sandino@sandino.net> (b45f4c261fba516ebac70d54469f0306ad6d64ae)
- Taraak (libre software enthusiast and activist), Murcia Spain. (Wed, 24 Mar 2021 23:26:10 +0100)

----
Organizational endorsements
----

The Free Software Force ( https://fsforce.noblogs.org ), born in September 2019 in response to the previous campaign of persecution against Richard Stallman, makes the following statement and endorses (and encourages endorsement of) the two letters of support for Richard Stallman that are collecting signatures (instructions in footnote*):

https://rms-support-letter.github.io/index.html

https://gitlab.com/KenjiBrown/rms-open-letter/-/blob/master/index.md (replicated in https://github.com/KenjiBrown/rms-open-letter.github.io/blob/main/index.md )

and suggests reading the following two articles:

https://jorgemorais.gitlab.io/justice-for-rms

https://www.wetheweb.org/post/cancel-we-the-web

Free Software Force statement on the campaign of persecution against Richard Stallman in 2021

The free computing movement is not only about defending free software, but also about defending free hardware, free information and freedom of speech. Anyone who claims to defend "free software" and does not defend freedom of speech, has not really understood what "free software" means. Defending freedom of speech is not only defending freedom of opinion for those who think the same as you on a subject, it is also defending the freedom to express their opinion to those who think differently and even contrary to you.

“Without freedom of thought, there can be no such thing as wisdom; and no such thing as publick liberty, without freedom of speech.” – Benjamin Franklin

“If liberty means anything at all, it means the right to tell people what they do not want to hear.” – George Orwell

“If we don't believe in freedom of expression for people we despise, we don't believe in it at all.” – Noam Chomsky

Unfortunately we are currently suffering a resurgence of ideological persecution by the liberticides of the self-proclaimed "one right thought", through a strategy dubbed "social cancellation culture" which means lynching and persecution of anyone who expresses any personal opinion contrary in any respect to the "one right thought". They forget that ethics and reasoning are spread through dialogue and argumentation and not through persecution and imposition. They forget that opinion diversity, discrepancy, debate and opposing criticism are essential for the improvement of our knowledge and as people. They forget that persecution under the banner of "right thinking" has led to repeated violations of human rights and freedoms throughout history. They forget that ultimately, no two people ever think alike on everything, so each can always use it as a weapon against his or her partner.

“First they came for the socialists, and I did not speak out,

because I was not a socialist.

Then they came for the trade unionists, and I did not speak out,

because I was not a trade unionist.

Then they came for the jews, and I did not speak out,

because I was not a jew.

Then they came for me,

and there was no one left to speak for me.”

– Martin Niemöller

* Instructions for signature:

For https://rms-support-letter.github.io/index.html

Send an email (preferably in text format, not HTML) with the following two lines ("name" and "link") to signrms@prog.cf and/or ~tyil/rms-support@lists.sr.ht

name: Free Software Force

link: https://fsforce.noblogs.org

Substitute "Free Software Force" for the name of the person or organisation, and "https://fsforce.noblogs.org" for your web address or email address.

For https://gitlab.com/KenjiBrown/rms-open-letter/-/blob/master/index.md (replicated in https://github.com/KenjiBrown/rms-open-letter.github.io/blob/main/index.md )

send an email with your name and optionally a brief mention of your relationship with free software, to sandino@sandino.net

-----

La Free Software Force ( https://fsforce.noblogs.org ), nacida en septiembre de 2019 en respuesta a la anterior campaña de persecución contra Richard Stallman, realiza la siguiente afirmación y suscribe (y anima a suscribir) las dos cartas de apoyo a Richard Stallman que están recogiendo firmas (instrucciones en nota al pie*):

https://rms-support-letter.github.io/index.html

https://gitlab.com/KenjiBrown/rms-open-letter/-/blob/master/index.md (replicada en https://github.com/KenjiBrown/rms-open-letter.github.io/blob/main/index.md )

y sugiere la lectura de los dos siguientes artículos:

https://jorgemorais.gitlab.io/justice-for-rms

https://www.wetheweb.org/post/cancel-we-the-web

Declaración de la Free Software Force ante la campaña de persecución contra Richard Stallman en 2021

El movimiento de la informática libre no es solo defender el software libre, sino también defender el hardware libre, la información libre y la libre expresión de opinión. Quien dice defender el “software libre” y no defiende la libertad de expresión, realmente no ha entendido qué significa “software libre”. Defender la libertad de expresión no es defender solo la libertad de opinión para quienes piensan igual que tú en un tema, es también defender la libertad de expresar su opinión a quien piensa diferente e incluso contrario a ti.

“Sin libertad de pensamiento, no puede haber tal cosa como la sabiduría; y no hay tal cosa como la libertad pública, sin libertad de expresión” – Benjamin Franklin

“Si la libertad significa algo, significa el derecho a decir a la gente lo que no quiere oír” – George Orwell

“Si no creemos en la libertad de expresión de las personas que despreciamos, no creemos en ella en absoluto.” – Noam Chomsky

Lamentablemente estamos actualmente sufriendo un resurgimiento de la persecución ideológica por los liberticidas del autoproclamado “único pensamiento correcto”, mediante una estrategia apodada “cultura de cancelación social” que significa linchamiento y persecución a toda persona que exprese alguna opinión personal contraria en algún aspecto al “único pensamiento correcto”. Olvidan que la ética y el razonamiento se difunde mediante el diálogo y la argumentación y no mediante la persecución y la imposición. Olvidan que la diversidad de opiniones, la discrepancia, el debate y la crítica opuesta son imprescindibles para la mejora de nuestro conocimiento y como personas. Olvidan que la persecución abanderada por el “pensamiento correcto” ha provocado reiteradas violaciones de los derechos y libertades humanas a lo largo de la historia. Olvidan que en última instancia, dos personas nunca piensan igual en todo, así que cada una siempre podrá utilizarla como arma contra su compañera.

“Primero vinieron por los socialistas, y yo no dije nada,

porque yo no era socialista.

Luego vinieron por los sindicalistas, y yo no dije nada,

porque yo no era sindicalista.

Luego vinieron por los judíos, y yo no dije nada,

porque yo no era judío.

Luego vinieron por mí, y no quedó nadie para hablar por mí.”

– Martin Niemöller

* Instrucciones para la firma:

Para https://rms-support-letter.github.io/index.html

Enviar un correo electrónico (preferiblemente en formato texto, no HTML) con las siguientes dos líneas (“name” y “link”) a signrms@prog.cf y/o ~tyil/rms-support@lists.sr.ht

name: Free Software Force

link: https://fsforce.noblogs.org

Sustituyendo “Free Software Force” por el nombre de la persona u organización, y “https://fsforce.noblogs.org” por tu dirección web o correo electrónico

Para https://gitlab.com/KenjiBrown/rms-open-letter/-/blob/master/index.md (replicada en https://github.com/KenjiBrown/rms-open-letter.github.io/blob/main/index.md )

enviar un correo electrónico con tu nombre y opcionalmente un breve mención a tu relación con el software libre, a sandino@sandino.net

----
